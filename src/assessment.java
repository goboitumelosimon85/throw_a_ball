
public class assessment {


    public static void main(String[] args) {
        //maxBall(15); // should return 4
        maxBall(25); // should return 7
    }

    public static int maxBall(int v) { // given velocity (km/h)
        // km to m, h to 0.1 sec
            double gravity = 9.81; // Earth's gravity constant 9.81 m/s²; sec to 0.1 sec

            double currentHight = 0; // current height (in meters) of ball at each tenths of a second
            double MaximumHight = 0; // the max height the ball has reached so far
            double time = 0; //current time in seconds
            int tans = 0; // tenths of a second
            while (true) {
                currentHight = (double)(v)*1000/60/60*time - 0.5*gravity*time*time;
                time += 0.1;

                if (currentHight < MaximumHight) {
                    return tans-1;

                }
                System.out.println(tans + ", " + currentHight);
                tans += 1;
                MaximumHight = currentHight;

    }


    }


}


